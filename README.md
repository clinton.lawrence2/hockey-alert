# Hockey Goal Alert

Simple Python Script that will allow you easily trigger lights and and Goal/Win Songs
Currently Manually Triggered Auto Triggering to be added later
NOTE: SONG/Keyboard SUPPORT IS CURRENTLY ONLY FOR WINDOWS -- If any one knows an easy way to play music and ready keyboard from linux I will update

## Pre Reqs
You must install the following to use
1) [Python](https://www.python.org/downloads/)
2) [pip](https://pypi.org/project/pip/) (If not installed with python)
3) IFTTT OR TUYA Setup 
   * https://ifttt.com/maker_webhooks
   * TUYA as discoverd by "codetheweb" and his work on the tuyaAPI:
      * https://github.com/codetheweb/tuyapi/blob/master/docs/SETUP.md

Once pip is installed, run pip against the requirements.txt to get all needed libraries
```bash
pip install -r requirements.txt
```

## Usage

Run the python script 

```bash
python main.py
```

### Log Example
```
INFO:root:Starting Keyboard Listener Loop
GOAL!!!!
INFO:root:GOAL!!!!
INFO:root:GOAL Song:C:\Users\123fa\hockey-alert\songs\ArizonaCoyotes\GoalHorn.wav
Goal Lights
STOP!!
resetting Lights
Got Stop Command Stopping Lights and Music
WE WIN!!!!
INFO:root:WIN!!!!
INFO:root:Win Song:C:\Users\123fa\hockey-alert\songs\ArizonaCoyotes\WinHorn.wav
INFO:root:WE WON!!!!
Win Lights
STOP!!
resetting Lights
Got Stop Command Stopping Lights and Music

```
## Files
### config.yml

This file holds some basic configs you will need to set accordingly

```yml

## Team you want to track
teamName: "Arizona Coyotes" 

## Location of Songs
songDirectory: "songs" 

#Which API Provider Are you Using (Must Choose 1)
### IFTTT
### TUYA (aka Smart Life)
apiProvider: "IFTTT"

#if using tuya provide your api keys
tuya_clientId: "1194d5958540300dwugh"
tuya_secret: "dac770f4c9964dd493f419f7ac1f4d91"

#if using IFTTT provide your API key/Webhook/Maker Key
ifttt_key: "ceOzKHV39iYlxKSENF8uyK"

## logging level you would like to set (INFO,WARN,DEBUG)
logLevel : "DEBUG"

##Input Type(Must Choose 1)
### keyboard - user presses keys on keyboard 'g','w','q' to control lights/song
### web - allows user to control lights/song by making API call to this app
inputType: "keyboard"

```
### main.py

Script to run either the Web API or event listening for keyboard

### requirements.txt

Python requirements file for downloading appropriate libraries to run the script

## Contributing
Pull requests are welcome.

Email: clinton.lawrence2@gmail.com


## License
[MIT](https://choosealicense.com/licenses/mit/)