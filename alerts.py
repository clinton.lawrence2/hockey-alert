#!/usr/bin/python3
import logging
import os
from threading import Event, Thread
from time import sleep

import pygame
import yaml

logging.basicConfig(level=logging.INFO)
cfg = None
dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path+"/config.yml", 'r') as ymlFile:
    cfg = yaml.safe_load(ymlFile)

if cfg["apiProvider"] == "TUYA":
    logging.info("Using TUYA to control lights")
    import tuya as lights

if cfg["apiProvider"] == "WIZ":
    logging.info("Using WIZ to control lights")
    import wiz as lights



teamName = cfg['teamName']
songDir = cfg ['songDirectory']
my_path = os.path.abspath(os.path.dirname(__file__))

pygame.mixer.init()


def goal():
    logging.info("GOAL!!!!")
    
    goalPath = os.path.join(my_path, songDir ,teamName.replace(" ",""),"GoalHorn.mp3" )
    pygame.mixer.music.load(goalPath)
    pygame.mixer.music.play()
    lights.goal()

def win():
    logging.info("WIN!!!!")
    
    my_path = os.path.abspath(os.path.dirname(__file__))
    winPath = os.path.join(my_path, songDir ,teamName.replace(" ",""),"WinHorn.mp3" )
    pygame.mixer.music.load(winPath)
    pygame.mixer.music.play()

    logging.info("WE WON!!!!")
    lights.win()

def stop():
    pygame.mixer.music.stop()
    lights.reset()
    logging.info('Got Stop Command Stopping Lights and Music')



    
