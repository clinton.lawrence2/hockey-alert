import asyncio
import logging 
from pywizlight.discovery import find_wizlights
import time

buildIps = []
async def main():
    bulbs = await find_wizlights(broadcast_address="192.168.86.255")
    print("ANY BULBS:"+str(bulbs))
    for bulb in bulbs:
        buildIps.append(bulb.ip_address)

def findLights():
        logging.basicConfig(level=logging.DEBUG)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main())
        time.sleep(10)
        return buildIps
    