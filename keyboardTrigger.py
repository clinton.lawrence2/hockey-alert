from pynput.keyboard import Key,Listener,KeyCode
import alerts
import yaml
import logging
import requests
from datetime import datetime,timedelta
from pytz import timezone
from time import sleep
import os

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path+"/config.yml", 'r') as ymlFile:
    cfg = yaml.safe_load(ymlFile)

logging.basicConfig(level=logging.INFO)

global runGoal
runGoal = True

def on_release(key):
    global runGoal
    if key == Key.esc:
        logging.info("Stopping Keyboard Listener and closing App")
        alerts.stop()
        return False
    elif key == Key.space:
        if runGoal:
            runGoal = False
            logging.info("GOAL!!!!")
            alerts.goal()
        else:
            runGoal = True
            logging.info("STOP!!")
            alerts.stop()
    elif key == KeyCode.from_char("w"):
        logging.info("WE WIN!!!!")
        alerts.win()
    else:
        logging.info("Unknown Key Press")

def loop():
    with Listener( on_release=on_release) as listener:
        listener.join()

logging.info("Now Listening to Keyboard...")
loop()
