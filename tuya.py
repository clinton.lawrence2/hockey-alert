import requests
import time
import calendar
import hashlib
import hmac
import base64
import json
import colorsys
from matplotlib import colors
from concurrent.futures import ThreadPoolExecutor
import logging
from time import sleep


logging.basicConfig(level=logging.DEBUG)

global stop
stop = True

def sign(clientId,secret,timestamp):
    message = '{}{}'.format(clientId, timestamp)
    signature = hmac.new(bytes(secret , 'latin-1'), msg = bytes(message , 'latin-1'), digestmod = hashlib.sha256).hexdigest().upper()
   
    return signature

def signWToken(clientId,secret,token,timestamp):
    message = '{}{}{}'.format(clientId,token,timestamp)
    signature = hmac.new(bytes(secret , 'latin-1'), msg = bytes(message , 'latin-1'), digestmod = hashlib.sha256).hexdigest().upper()
    
    return signature

def getHeaders():
    epoch = int(time.time() * 1000)
    clientId = "1194d5958540300dwugh"
    secret = "dac770f4c9964dd493f419f7ac1f4d91"
    sig = sign(clientId,secret,epoch)
    headers = {'client_id':clientId ,'access_token': secret,'t':str(epoch),"sign": sig,"sign_method":"HMAC-SHA256"}

    response = requests.get(url="https://openapi.tuyaus.com/v1.0/token?grant_type=1",headers=headers) 

    tokenJson = response.json()
    token = tokenJson["result"]["access_token"]

    sigToken = signWToken(clientId,secret,token,epoch)

    headers2 = {'client_id':clientId ,'access_token': token,'t':str(epoch),"sign": sigToken,"sign_method":"HMAC-SHA256","lang":"en","schema":"hockeyalert"}
    return headers2

def getStatus(deviceId):
    try:
        logging.info("Getting Current Status")
        headers = getHeaders()
        statusUrl = "https://openapi.tuyaus.com/v1.0/devices/"+deviceId+"/status"
        statusJson = requests.get(url=statusUrl,headers = headers)
        statusJson = statusJson.json()
        for result in statusJson["result"]:
            if result["code"] == "switch_led":
                isItOn = result["value"] 
                return isItOn
    except expression:
        raise expression
    
def pumpLoop(deviceList):
    logging.info("Pump It Up")
    global stop
    introSleep = 33
    sirenSleep = 36
    for deviceId in deviceList: 
        changeState(deviceId,"off")

    sleep(introSleep)   
    for deviceId in deviceList:
        if not stop:
            changeState(deviceId,"on")
            changeColor(deviceId,"red")
            sleep(1)
            changeState(deviceId,"off")
        else:
            break
    
    sleep(4)
    futureList = []
    executor = ThreadPoolExecutor(max_workers=len(deviceList))
    for device in deviceList:
        futureList.append(executor.submit(goalLoop, device))
    

    stop = False
    sleep(sirenSleep)
    stop = True
    
    for deviceId in deviceList:
        changeColor(deviceId=deviceId,color="white")
        changeState(deviceId,"off")
    

def goalLoop(deviceId):
    isItOn = getStatus(deviceId)
    logging.info(deviceId+" ON:"+ str(isItOn))
    if not isItOn:
        changeState(deviceId,"on")
    changeColor(deviceId,"red")
    while not stop:
        changeColor(deviceId,"dark_red")
        changeColor(deviceId,"red")
   
    logging.info("deviceId:"+str(deviceId)+" outside of light loop; reset")
    changeColor(deviceId=deviceId,color="white")
    if not isItOn:
        changeState(deviceId,"off")

def winLoop(deviceId):
    isItOn = getStatus(deviceId)
    if not isItOn:
        changeState(deviceId,"on")
    changeColor(deviceId,"red")
    while not stop:
        changeState(deviceId,"off")
        sleep(1)
        changeState(deviceId,"on")
        sleep(1)
    
    logging.info("deviceId:"+str(deviceId)+" outside of light loop; reset")
    changeColor(deviceId=deviceId,color="white")
    if not isItOn:
        changeState(deviceId,"off")

def changeState(deviceId,state):
    
    commandJson = None
    
    newState = True if state == "on" else False

    commandJson = {'commands':[ {'code':'switch_led','value': newState }]}

    headers2 = getHeaders()
    print("device ID:"+deviceId+" setting state to:"+state)
    commandURL = "https://openapi.tuyaus.com/v1.0/devices/"+deviceId+"/commands"
    commandResponse = requests.post(url=commandURL,headers = headers2, json = commandJson)
    

def changeColor(deviceId,color):
    
    commandJson = None

    RED_HSV=[0,255,255]
    DARKRED_HSV=[0,255,25]

    HSV = []


    if color == "white":
        commandJson = {"commands":[ {"code": "work_mode","value": "white"}]}
    else:
        if color == "red":
            HSV = RED_HSV
        elif color == "dark_red":
            HSV = DARKRED_HSV
        
        h = str(HSV[0])
        s = str(HSV[1])
        v = str(HSV[2])
        commandJson = {"commands":[ {"code": "work_mode","value": "colour"},{"code":"colour_data","value": "{\"h\":"+h+",\"s\":"+s+",\"v\":"+v+"}"}]}
    
    print("changing light:" +deviceId+" to color:" +color)
    commandURL = "https://openapi.tuyaus.com/v1.0/devices/"+deviceId+"/commands"
    
    headers2 = getHeaders() 
    commandResponse = requests.post(url=commandURL,headers = headers2, json = commandJson)
    print(commandResponse.text)




def getDevices(label):

    headers2 = getHeaders()
    uID = "az1543376701508AxNGr"
    getDevices = "https://openapi.tuyaus.com/v1.0/users/"+uID+"/devices"

    response = requests.get(url=getDevices,headers=headers2)

    devices = response.json()["result"]


    deviceList = []


    for device in devices:
        if label in device["name"]:
            deviceList.append(device["id"])
    return deviceList



def goal():
    logging.info("tuya goal")
    global stop 
    stop = False
    futureList = []
    deviceList = getDevices("Entry")
    executor = ThreadPoolExecutor(max_workers=len(deviceList))
    for device in deviceList:
        futureList.append(executor.submit(goalLoop, device))  

def win():
    global stop 
    stop = False
    futureList = []
    deviceList = getDevices("LivingRoom")
    executor = ThreadPoolExecutor(max_workers=len(deviceList))
    for device in deviceList:
        futureList.append(executor.submit(winLoop, device))  


def start():
    deviceList = getDevices("LivingRoom")
    for deviceId in deviceList:
        stop = True
        changeState(deviceId,"on")
        changeColor(deviceId,"white")

def reset():
    global stop
    stop = True


