import asyncio
from random import uniform
import socket
import time
import logging
from concurrent.futures import ThreadPoolExecutor
from pywizlight import wizlight, PilotBuilder, discovery
import multiprocessing as mp
from findWiz import findLights

logging.basicConfig(level=logging.DEBUG)

global stop
stop = True

sock = socket.socket(socket.AF_INET, # Internet
                            socket.SOCK_DGRAM) # UDP
logging.info("Finding Lights....")
ipList = findLights()

logging.info("LightIPs:"+str(ipList))

UDP_PORT = 38899
RED_MESSAGE = b'{"id":1,"method":"setPilot","params":{"r":255,"g":0,"b":0,"dimming":100}}'
PURPLE_MESSAGE = b'{"id":1,"method":"setPilot","params":{"r":255,"g":0,"b":255,"dimming":100}}'
GREEN_MESSAGE = b'{"id":1,"method":"setPilot","params":{"r":0,"g":255,"b":0,"dimming":100}}'
OFF_MESSAGE = b'{"id":1,"method":"setState","params":{"state":false}}'

def win():
    logging.info("WIZ win")
    global stop 
    stop = False
    
    futureList = []
    executor = ThreadPoolExecutor(max_workers=len(ipList))
    for ip in ipList:
        futureList.append(executor.submit(winLoop,ip))

def goal():
    logging.info("WIZ goal")
    global stop 
    stop = False
    futureList = []
    executor = ThreadPoolExecutor(max_workers=len(ipList))
    for ip in ipList:
        logging.info("adding future for ip "+ ip)
        futureList.append(executor.submit(goalLoop,ip))
        time.sleep(1)
        logging.info("Done adding future for ip "+ ip)

def winLoop(ip):
    logging.info("win loop")
    
    # Set RGB values
    while not stop:
        sock.sendto(RED_MESSAGE, (ip, UDP_PORT))
        randSleepeep1 = uniform(1, 2.5)
        time.sleep(randSleepeep1)

        sock.sendto(PURPLE_MESSAGE, (ip, UDP_PORT))
        randSleepeep2 = uniform(1, 2.5)
        time.sleep(randSleepeep2)

        sock.sendto(GREEN_MESSAGE, (ip, UDP_PORT))
        randSleepeep3 = uniform(1, 2.5)
        time.sleep(randSleepeep3)
        
    # Turns the light off
    sock.sendto(OFF_MESSAGE, (ip, UDP_PORT))

def goalLoop(ip):
    while not stop:
        sock.sendto(RED_MESSAGE, (ip, UDP_PORT))
        time.sleep(.5)

        sock.sendto(OFF_MESSAGE, (ip, UDP_PORT))
        time.sleep(.5)
    
    sock.sendto(OFF_MESSAGE, (ip, UDP_PORT))
        

def reset():
    logging.info("WIZ RESET")
    global stop
    stop = True



    

    




    
    